import Vue from 'vue'
import App from './App.vue'


import router from './router'
import store from './api'

import DisableAutocomplete from 'vue-disable-autocomplete';

import BootstrapVue from "bootstrap-vue"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import VeeValidate from 'vee-validate'
import VueCrontab  from 'vue-crontab'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCalendar, faGraduationCap, faUsers, faVials, faChalkboardTeacher, faFlask, faFileAlt, faSignOutAlt} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faCalendar, faGraduationCap, faUsers, faVials, faChalkboardTeacher, faFlask, faFileAlt, faSignOutAlt)

import VueGoodTablePlugin from 'vue-good-table';

// import the styles
import 'vue-good-table/dist/vue-good-table.css'

import "vue-easytable/libs/theme-default/index.css"; // import style
import VueEasytable from "vue-easytable"; // import library

VueCrontab.setOption({
  interval: 200,
  auto_start: false
})

Vue.use(DisableAutocomplete);
Vue.config.productionTip = false;
Vue.use(VueCrontab);
Vue.use(BootstrapVue);
Vue.use(VueEasytable);
Vue.use(VeeValidate);
Vue.use(VueGoodTablePlugin);
Vue.component('font-awesome-icon', FontAwesomeIcon);

new Vue({
  router,
  store,

  render: h => h(App)
}).$mount('#app');
