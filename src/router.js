import Vue                  from 'vue'
import Router               from 'vue-router'

import Login                from "@/components/LogIn";

import AdminHome            from "@/components/admin/AdminHome";
import AdminUsers           from "@/components/admin/AdminUsers";
import AdminRegister        from "@/components/admin/AdminRegister";

import User                 from "@/components/user/User";
import UserSupport          from "@/components/user/UserSupport";

import Definition           from "@/components/definition/Definition";
import DefinitionCourse     from "@/components/definition/DefinitionCourse";
import DefinitionSchool     from "@/components/definition/DefinitionSchool";

import Statistic            from "@/components/Statistic";

import Home                 from "@/components/Home";
import Calendar             from "@/components/Calendar";
import Student              from "@/components/Student";
import Courses              from "@/components/Courses";

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base:  process.env.BASE_URL,
    routes: [
        // LOGIN
        {
            path: '/login',
            name: 'Login von Natlab',
            components: {
                a: Login //you sidebar main component in 'a' name of routed-view
            },
        },

        // ADMIN VIEWS
        {
            path: '/admin',
            name: 'admin',
            redirect: "/admin/users",
            components: {
                a: AdminHome //you sidebar main component in 'a' name of routed-view
            },
            children: [
                {
                    path: '/admin/users',
                    name: 'adminUsers',
                    components: {
                        b: AdminUsers
                    }
                },
                {
                    path: '/admin/register',
                    name: 'adminRegister',
                    components: {
                        b: AdminRegister
                    }
                }
            ]
        },

        // MAIN CONTENT
        {
            path: '/',
            redirect: "courses",
            components: {
                a: Home //you sidebar main component in 'a' name of routed-view
            },
            children: [
                {
                    path: '/user',
                    redirect: "/user/support",
                    components: {
                        b: User
                    },
                    children: [
                        {
                            path: '/user/support',
                            name: 'support',
                            components: {
                                c: UserSupport
                            }
                        },
                    ]
                },

                {
                    path: '/courses',
                    name: 'courses',
                    components: {
                        b: Courses
                    }
                },

                {
                    path: '/definition',
                    redirect: "/definition/course",
                    components: {
                        b: Definition
                    },
                    children: [
                        {
                            path: '/definition/course',
                            name: 'definitioncourse',
                            components: {
                                c: DefinitionCourse
                            }
                        },
                        {
                            path: '/definition/school',
                            name: 'definitionschool',
                            components: {
                                c: DefinitionSchool
                            }
                        }
                    ]
                },

                {
                    path: '/statistic',
                    name: 'statistic',
                    components: {
                        b: Statistic
                    }
                },

                {
                    path: '/students',
                    name: 'students',
                    components: {
                        b: Student
                    }
                },

                {
                    path: '/calendar',
                    name: "calendar",
                    components: {
                        b: Calendar
                    }
                }
            ]
        },

    ]
})

router.beforeEach((to, from, next) => {
    const publicPages = ['/login'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');
    //const userStatus = localStorage.getItem('userStatus');

    // trying to access a restricted page + not logged in
    // redirect to login page
    if (authRequired && !loggedIn) {
        next('/login');
    } else {
        next();
    }
});

export default router