export default class Certification {

    constructor(id, certificationid, certificationtype, certificationname, certificationowner, certificationdate, certificationexpiredate, status) {
        this.id = id;
        this.certificationid = certificationid;
        this.certificationtype = certificationtype;
        this.certificationname = certificationname;
        this.certificationowner = certificationowner;
        this.certificationdate = certificationdate;
        this.certificationexpiredate = certificationexpiredate;
        this.status = status;
    }
}
