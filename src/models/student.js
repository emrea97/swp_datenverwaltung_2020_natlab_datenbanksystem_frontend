export default class Student {

    constructor(id, anrede, firstname, lastname, street, zip, city, sommeruni17, sommeruni18, sommeruni19, sommeruni20, natuerlich20, status) {
        this.id = id;
        this.anrede = anrede;
        this.firstname = firstname;
        this.lastname = lastname;
        this.street = street;
        this.zip = zip;
        this.city = city;
        this.sommeruni17 = sommeruni17;
        this.sommeruni18 = sommeruni18;
        this.sommeruni19 = sommeruni19;
        this.sommeruni20 = sommeruni20;
        this.natuerlich20 = natuerlich20;
        this.status = status;

    }
}
