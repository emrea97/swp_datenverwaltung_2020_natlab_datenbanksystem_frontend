export default class User {

    constructor(uid, username, firstname, lastname, email, password, status) {
        this.uid = uid;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.status = status;
    }

}