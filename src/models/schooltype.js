export default class CourseType {

    constructor(stid, stsnumber, stsname, ststype, stsadress, stszip, stsplace, stsdistrict, ststatus) {
        this.stid        = stid;
        this.stsnumber   = stsnumber;
        this.stsname     = stsname;
        this.ststype     = ststype;
        this.stsadress   = stsadress;
        this.stszip      = stszip;
        this.stsplace    = stsplace;
        this.stsdistrict = stsdistrict;
        this.ststatus    = ststatus;
    }

}
