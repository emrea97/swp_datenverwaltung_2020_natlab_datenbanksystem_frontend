export default class CourseType {

    constructor(ctid, ctname, cttype, ctsubject, ctparticipation, ctsupport, ctcare, ctstatus) {
        this.ctid            = ctid;
        this.ctname          = ctname;
        this.cttype          = cttype;
        this.ctsubject       = ctsubject;
        this.ctparticipation = ctparticipation;
        this.ctsupport       = ctsupport;
        this.ctcare          = ctcare;
        this.ctstatus        = ctstatus;
    }
}
