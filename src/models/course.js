export default class Course {

    constructor(cid, cnumber, cname, cctid, cdate, ctime, ctname, ctmail, ctphone, cstid, cclasslevel, camountpresent, camountregistrations, cfee, cstatus) {
        this.cid                  = cid;
        this.cnumber              = cnumber;
        this.cname                = cname;
        this.cctid                = cctid;
        this.cdate                = cdate;
        this.ctime                = ctime;
        this.ctname               = ctname;
        this.ctmail               = ctmail;
        this.ctphone              = ctphone;
        this.cstid                = cstid;
        this.cclasslevel          = cclasslevel;
        this.camountpresent       = camountpresent;
        this.camountregistrations = camountregistrations;
        this.cfee                 = cfee;
        this.cstatus              = cstatus;
    }
}
