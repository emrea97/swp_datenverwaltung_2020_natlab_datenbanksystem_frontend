export default class Support {

    constructor(sid, snameFirst, snameLast, supportworked) {
        this.sid           = sid;
        this.snameFirst    = snameFirst;
        this.snameLast     = snameLast;
        this.supportworked = supportworked;
    }
}
