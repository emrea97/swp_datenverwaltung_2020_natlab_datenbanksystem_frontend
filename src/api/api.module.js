/**
 * Hier werden die API-Requests aus den Componenten definiert.
 * this.$store.dispatch('api/....')
 * Von hier aus werden die gleichnamigen Methoden aus dem ApiService aufgerufen.
 *
 */

import ApiService     from '@/api/api.service';

import User           from "@/models/user";

const user = JSON.parse(localStorage.getItem('user'));
const initialState =
    user ? {
        status: {
            loggedIn: true
        },
        user
    } : {
        status: {
            loggedIn: false
        },
        user: new User("", "", "")
    };

/* eslint-disable no-unused-vars */
export const api = {
    namespaced: true,
    state: initialState,
    actions: {

//////////// ADMIN ////////////
    login({ commit }, user) {
        return ApiService.login(user).then(
            user => {
                commit('loginSuccess', user);
                return Promise.resolve(user);
            },
            error => {
                commit('loginFailure');
                return Promise.reject(error);
            }
        );
    },

    logout({ commit }) {
        ApiService.logout();
        commit('logout');
    },

    register({ commit }, user) {
        return ApiService.register(user).then(
            response => {
                commit('registerSuccess');
                return Promise.resolve(response.data);
            },
            error => {
                commit('registerFailure');
                return Promise.reject(error);
            }
        );
    },

    getUsers({ commit }) {
        return ApiService.getUsers().then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    editUser({ commit }, user) {
        return ApiService.editUser(user).then(
            response => {
                commit('registerSuccess');
                return Promise.resolve(response.data);
            },
            error => {
                commit('registerFailure');
                return Promise.reject(error);
            }
        );
    },

    deleteUser({ commit }, user) {
        return ApiService.deleteUser(user).then(
            response => {
                commit('registerSuccess');
                return Promise.resolve(response.data);
            },
            error => {
                commit('registerFailure');
                return Promise.reject(error);
            }
        );
    },

    isExpired({ commit }, user) {
        return ApiService.isExpired(user).then(
            response => {
                commit('isExpiredSuccess', response);
                return Promise.resolve(response);
            },
            error => {
                commit('isExpiredFailure');
                return Promise.reject(error);
            }
        );
    },

//////////// SUPPORT ////////////
    getSupports({ commit }) {
        return ApiService.getSupports().then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    addSupport({ commit }, support) {
        return ApiService.addSupport(support).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    updateSupport({ commit }, support) {
        return ApiService.updateSupport(support).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    getSupportWorked({ commit }) {
        return ApiService.getSupportWorked().then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    addSupportWorked({ commit }, supportworked) {
        return ApiService.addSupportWorked(supportworked).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    delSupportWorked({ commit }, supportworked) {
        return ApiService.delSupportWorked(supportworked).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    exportSupportWorked({ commit }, data) {
        return ApiService.exportSupportWorked(data).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

//////////// COURSE ////////////
    getCourses() {
        return ApiService.getCourses().then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    addCourse({ commit }, course) {
        return ApiService.addCourse(course).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    updateCourse({ commit }, course) {
        return ApiService.updateCourse(course).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    exportCourse({ commit },ids) {
        return ApiService.exportCourse(ids).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

//////////// COURSETYPE ////////////
    getCourseTypes() {
        return ApiService.getCourseTypes().then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    addCourseType({ commit }, coursetype) {
        return ApiService.addCourseType(coursetype).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    updateCourseType({ commit }, coursetype) {
        return ApiService.updateCourseType(coursetype).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    exportCourseType({ commit }, ids) {
        return ApiService.exportCourseType(ids).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

//////////// SCHOOLTYPE ////////////
    getSchoolTypes() {
        return ApiService.getSchoolTypes().then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    addSchoolType({ commit }, schooltype) {
        return ApiService.addSchoolType(schooltype).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    updateSchoolType({ commit }, schooltype) {
        return ApiService.updateSchoolType(schooltype).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    exportSchoolType({ commit }, ids) {
        return ApiService.exportSchoolType(ids).then(
            response => { return Promise.resolve(response); },
            error    => { return Promise.reject(error); }
        );
    },

    },

    mutations: {
        loginSuccess(state, user) {
            state.status.loggedIn = true;
            state.user = user;
        },
        loginFailure(state) {
            state.status.loggedIn = false;
            state.user = null;
        },
        logout(state) {
            state.status.loggedIn = false;
            state.user = null;
        },
        registerSuccess(state) {
            state.status.loggedIn = false;
        },
        registerFailure(state) {
            state.status.loggedIn = false;
        },
        isExpiredSuccess(state, isExpired) {
            state.status.isExpired = isExpired;
        },
        isExpiredFailure(state, isExpired) {
            console.log("error state")
            state.status.isExpired = isExpired;
        }
    }
};