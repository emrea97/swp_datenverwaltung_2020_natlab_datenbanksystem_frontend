/**
 * Dient zur Authorization der API-Requests
 *
 * @returns {{Authorization: string}|{}}
 */
export default function authHeader() {
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.idToken) {
        return { Authorization: 'Bearer ' + user.idToken };
    } else {
        console.log("Header fialure");
        return {}
    }
}