/**
 * Von hier aus werden API-Requests an das Backend gesendet.
 * Bei Post-Requests werden Parameter mitgesendet die sich in data{} befinden.
 * Im Backend sind diese Parameter im RequestBody.
 */

import axios from 'axios';
import authHeader from "@/api/auth-header";
import fileDownload from "js-file-download";

const API_URL = 'http://localhost:9192/';

class ApiService {

//////////// ADMIN ////////////
    login(user) {
        return axios
            .post(API_URL + 'authenticate', {
                username: user.username,
                password: user.password
            })
            .then(response => {
                if (response.data.idToken) {
                    localStorage.setItem('user', JSON.stringify(response.data));
                    localStorage.setItem('cronJob', 0);
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_URL + 'register', {
            username  : user.username,
            firstname : user.firstname,
            lastname  : user.lastname,
            email     : user.email,
            password  : user.password
        },{ headers: authHeader() });
    }

    getUsers(){
        return axios
            .get(API_URL + 'getUsers', {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    editUser(user) {
        return axios.post(API_URL + 'editUser', {
            uid       : user.uid,
            username  : user.username,
            firstname : user.firstname,
            lastname  : user.lastname,
            email     : user.email,
            password  : user.password
        },{ headers: authHeader() });
    }

    deleteUser(user) {

        if (user.status === 0)
            user.status = 1
        else
            user.status = 0

        return axios.post(API_URL + 'deleteUser', {
            uid       : user.uid,
            status    : user.status
        },{ headers: authHeader() });
    }

    isExpired(user) {
        return axios.post(API_URL + 'isExpired', {
                expiresIn: user.expiresIn
        });
    }

//////////// COURSE ////////////
    getCourses() {
        return axios
            .get(API_URL + 'getCourses', {
                headers: authHeader()
            })
            .then(response => {
                return response.data;
            });
    }

    addCourse(course) {
        return axios.post(API_URL + 'addCourse', {
            cnumber              : course.cnumber,
            cname                : course.cname,
            cctid                : course.cctid,
            cdate                : course.cdate,
            ctime                : course.ctime,
            ctname               : course.ctname,
            ctmail               : course.ctmail,
            ctphone              : course.ctphone,
            cstid                : course.cstid,
            cclasslevel          : course.cclasslevel,
            camountpresent       : course.camountpresent,
            camountregistrations : course.camountregistrations,
            cfee                 : course.cfee
            },{headers: authHeader() }
        ).then(response => {
                return response.data;
            });
    }

    updateCourse(course) {
        return axios.post(API_URL + 'updateCourse', {
                    cid                  : course.cid,
                    cnumber              : course.cnumber,
                    cname                : course.cname,
                    cctid                : course.cctid,
                    cdate                : course.cdate,
                    ctime                : course.ctime,
                    ctname               : course.ctname,
                    ctmail               : course.ctmail,
                    ctphone              : course.ctphone,
                    cstid                : course.cstid,
                    cclasslevel          : course.cclasslevel,
                    camountpresent       : course.camountpresent,
                    camountregistrations : course.camountregistrations,
                    cfee                 : course.cfee
                },{headers: authHeader() }
                ).then(response => {
                    return response.data;
                });
    }

    deleteCourse(id) {
        return axios
            .post(API_URL + 'deleteCourse', {
                    id: id
                }
                ,{headers: authHeader() })
            .then(response => {
                return response.data;
            });
    }

    exportCourse(ids) {
        let idsArray = ""
        if (ids.length > 0)
            idsArray =  "/";

        for (let i = 0; i < ids.length; i++) {
            if (i === ids.length-1)
                idsArray += ids[i];
            else
                idsArray += ids[i]+",";
        }

        return axios
            .get(API_URL + 'exportCourseExcel' + idsArray, {
                headers: authHeader(),
                responseType: 'blob',
            })
            .then(response => {
                fileDownload(response.data, "Kurse.xlsx");
                return response.data;
            });
    }

//////////// COURSETYPE ////////////
    getCourseTypes() {
        return axios
            .get(API_URL + 'getCourseTypes', { headers: authHeader() })
            .then(response => { return response.data; });
    }

    addCourseType(coursetype) {
        return axios.post(API_URL + 'addCourseType', {
            ctname           : coursetype.ctname,
            cttype           : coursetype.cttype,
            ctsubject        : coursetype.ctsubject,
            ctparticipation  : coursetype.ctparticipation,
            ctsupport        : coursetype.ctsupport,
            ctcare           : coursetype.ctcare
            },{headers: authHeader() }
        ).then(response => {
            return response.data;
        });
    }

    updateCourseType(coursetype) {
        return axios.post(API_URL + 'updateCourseType', {
            ctid             : coursetype.ctid,
            ctname           : coursetype.ctname,
            cttype           : coursetype.cttype,
            ctsubject        : coursetype.ctsubject,
            ctparticipation  : coursetype.ctparticipation,
            ctsupport        : coursetype.ctsupport,
            ctcare           : coursetype.ctcare
            },{headers: authHeader() }
        ).then(response => {
            return response.data;
        });
    }

    exportCourseType(ids) {
        let id = (ids.length === 0) ? "-1" : ids.join();
        return axios
            .get(API_URL + 'exportCourseType/' + id, {
                headers: authHeader(),
                responseType: 'blob',
            })
            .then(response => {
                fileDownload(response.data, "Kursdefinition.xlsx");
                return response.data;
            });
    }

//////////// SCHOOLTYPE ////////////
    getSchoolTypes() {
        return axios
            .get(API_URL + 'getSchoolTypes', { headers: authHeader() })
            .then(response => { return response.data; });
    }

    addSchoolType(schooltype) {
        return axios.post(API_URL + 'addSchoolType', {
                stsnumber   : schooltype.stsnumber,
                stsname     : schooltype.stsname,
                ststype     : schooltype.ststype,
                stsadress   : schooltype.stsadress,
                stszip      : schooltype.stszip,
                stsplace    : schooltype.stsplace,
                stsdistrict : schooltype.stsdistrict
            },{headers: authHeader() }
        ).then(response => {
            return response.data;
        });
    }

    updateSchoolType(schooltype) {
        return axios.post(API_URL + 'updateSchoolType', {
                stid        : schooltype.stid,
                stsnumber   : schooltype.stsnumber,
                stsname     : schooltype.stsname,
                ststype     : schooltype.ststype,
                stsadress   : schooltype.stsadress,
                stszip      : schooltype.stszip,
                stsplace    : schooltype.stsplace,
                stsdistrict : schooltype.stsdistrict
            },{headers: authHeader() }
        ).then(response => {
            return response.data;
        });
    }

    exportSchoolType(ids) {
        let id = (ids.length === 0) ? "-1" : ids.join();
        return axios
            .get(API_URL + 'exportSchoolType/' + id, {
                headers: authHeader(),
                responseType: 'blob',
            })
            .then(response => {
                fileDownload(response.data, "Schuldefinition.xlsx");
                return response.data;
            });
    }

//////////// SUPPORT ////////////
    getSupports() {
        return axios
            .get(API_URL + 'getUserSupports', { headers: authHeader() })
            .then(response => {  return response.data; });
    }

    addSupport(support) {
        return axios.post(API_URL + 'addUserSupport', {
                snameFirst : support.snameFirst,
                snameLast  : support.snameLast
            },{headers: authHeader() }
        ).then(response => { return response.data; });
    }

    updateSupport(support) {
        return axios.post(API_URL + 'updateUserSupport', {
                sid        : support.sid,
                snameFirst : support.snameFirst,
                snameLast  : support.snameLast
            },{headers: authHeader() }
        ).then(response => { return response.data; });
    }

    getSupportWorked() {
        return axios
            .get(API_URL + 'getSupportWorked', { headers: authHeader() })
            .then(response => {  return response.data; });
    }

    addSupportWorked(supportworked) {
        return axios.post(API_URL + 'addSupportWorked', {
                swsid     : supportworked.swsid,
                swamount  : supportworked.swamount,
                swdate    : supportworked.swdate
            },{headers: authHeader() }
        ).then(response => {
            return response.data;
        });
    }

    delSupportWorked(supportworked) {
        return axios.post(API_URL + 'delSupportWorked', {
                swid     : supportworked.swid
            },{headers: authHeader() }
        ).then(response => {
            return response.data;
        });
    }

    exportSupportWorked(data) {
        let id = (data.key1.length === 0) ? "-1" : data.key1.join();

        return axios
            .get(API_URL + 'exportSupportWorked/' + id +"/"+ data.key2, {
                headers: authHeader(),
                responseType: 'blob',
            })
            .then(response => {
                fileDownload(response.data, "Betreuung.xlsx");
                return response.data;
            });
    }

}

export default new ApiService();
