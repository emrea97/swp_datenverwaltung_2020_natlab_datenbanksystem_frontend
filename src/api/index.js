import Vue  from 'vue';
import Vuex from 'vuex';

import { api }           from '@/api/api.module';
import { event }         from '@/api/event.module';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        api,
        event
    }
});