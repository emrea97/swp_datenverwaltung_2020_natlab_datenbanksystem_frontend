# Nützliche Dokumente
Alle nützlichen Dokumente findet man [hier](https://git.imp.fu-berlin.de/emrea97/swp_datenverwaltung_2020_natlab_datenbanksystem_frontend/-/tree/master/documents)

# Benutzte Bibliotheken
[Tabelle](http://doc.huangsw.com/vue-easytable/#/en/doc/intro)  
[Kalendar](https://kalendar.altinselimi.com/?ref=madewithvuejs.com#getting-started)



# Implemented

- [x] Benutzeranmeldung | Benutzererstellung | Benutzerverwaltung
- [x] Kursübersicht (Hinzufügen, Editieren, Exportieren, Suchen, Filtern)
- [x] Übersicht zur studentischen Betreuung (Hinzufügen, Editieren, Exportieren, Suchen, Filtern)
- [x] Kursdefinitionen (Hinzufügen, Editieren, Exportieren, Suchen, Filtern)
- [x] Schuldefinitionen (Hinzufügen, Editieren, Exportieren, Suchen, Filtern)

# Need to know

Um die Benutzer zu verwalten gibt es einen Admin-Zugang mit den Zugangsdaten:
| Benutzername  | Passwort      |
| ------------- |:-------------:|
| admin         | password      |
